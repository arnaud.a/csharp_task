﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskClassLibrary
{
    public class Compteur
    {
        private int nombre;

        public Compteur()
        {
            nombre = 0;
        }
        public string incremente()
        {
            this.nombre = this.nombre + 1;
            return string.Format("{0}",nombre);
        }

        public string decremente()
        {
            this.nombre = this.nombre - 1;
            return string.Format("{0}", nombre);
        }

        public string remiseAZero()
        {
            this.nombre = 0;
            return string.Format("{0}", nombre);
        }

        public int getCompteur()
        {
            return nombre;
        }

        public void setCompteur(int nombretoSet)
        {
            nombre = nombretoSet;
        }
    }
}
