﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskClassLibrary;
using System.Windows.Forms;

namespace TaskProject
{
    public partial class Form1 : Form
    {
        Compteur ptitCompteur;
        public Form1()
        {
            InitializeComponent();
            this.ptitCompteur = new Compteur();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            label2.Text = ptitCompteur.remiseAZero();
        }

        private void Label2_Click(object sender, EventArgs e)
        {
           
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            label2.Text = ptitCompteur.incremente();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            label2.Text = ptitCompteur.decremente();
        }
    }
}
