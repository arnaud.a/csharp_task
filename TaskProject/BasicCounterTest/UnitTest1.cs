﻿using System;
using TaskClassLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BasicCounterTest
{
    [TestClass]
    public class CounterTest
    {
        [TestMethod]
        public void TestConstructeur()
        {
            Compteur compteur_test = new Compteur();
            Assert.AreEqual(0, compteur_test.getCompteur());
        }
        [TestMethod]
        public void TestIncremente()
        {
            Compteur compteur_test = new Compteur();
            Assert.AreEqual("1", compteur_test.incremente());
        }
        [TestMethod]
        public void TestDecremente()
        {
            Compteur compteur_test = new Compteur();
            compteur_test.setCompteur(2);
            Assert.AreEqual("1", compteur_test.decremente());
        }

        [TestMethod]
        public void TestRàZ()
        {
            Compteur compteur_test = new Compteur();
            compteur_test.setCompteur(4);
            Assert.AreEqual("0", compteur_test.remiseAZero());
        }
    }
}
